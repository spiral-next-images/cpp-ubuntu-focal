FROM ubuntu:20.04

RUN apt-get update && \
  apt-get install -y --no-install-recommends curl gcc g++ libc-dev make git ca-certificates zip unzip tar && \
  rm -rf /var/lib/apt/lists/* && \
  mkdir -p /app && \
  curl -kLo /app/cmake.tar.gz 'https://github.com/Kitware/CMake/releases/download/v3.24.0/cmake-3.24.0-linux-x86_64.tar.gz' && \
  cd /app && \
  mkdir cmake && \
  tar -C cmake --strip-components 1 -xvf cmake.tar.gz && \
  rm -f cmake.tar.gz && \
  ln -s /app/cmake/bin/cmake /bin/cmake
